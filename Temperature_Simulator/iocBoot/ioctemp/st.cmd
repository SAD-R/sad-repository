#!../../bin/linux-x86_64/temp

## You may have to change temp to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/temp.dbd"
temp_registerRecordDeviceDriver pdbbase

## Configure devices
epicsEnvSet ("STREAM_PROTOCOL_PATH", ".:../protocols")

##Line below for MCAG-EPICS6: 
drvAsynIPPortConfigure("PortA","192.168.88.181:9999",0,0,0)

## Load record instances
dbLoadRecords("db/temp.db","user=ikergonzalezHost")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=ikergonzalezHost"
