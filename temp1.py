#!/usr/bin/python
# Simulate a message-based device
#
import SocketServer
import math
import os
import random
import sys

def clip(v):
    if (v > 400.0): return 400
    if (v < 0.0): return 0
    return v

status ={}
class DummyDevice(SocketServer.StreamRequestHandler):
    def handle(self):
        global status
   
        client = self.client_address[0]
        if (not status.has_key(client)):
            status[client] = { }
            status[client]['celsius'] = 22.000
            status[client]['kelvin'] = 295.000
            status[client]['setp'] = 300.000
            status[client]['range'] = 0
            status[client]['p'] = 100.00
            status[client]['i'] = 50.00
            status[client]['d'] = 0.00
            status[client]['nameA'] = 'InputA'
            status[client]['nameB'] = 'InputB'
            status[client]['limit'] = 400.00

        while 1:
            line = self.rfile.readline().strip()
            args = line.split()
            if(len(line) <= 0):
               break
            reply = None
            if (line == '*IDN?'):
                reply = 'LSCI,MODEL336,336AB9U/306A0E5,2.5'
            elif (line == 'CRDG?'):
                reply = '%.3f' % (status[client]['celsius'])
            elif (line == 'KRDG?'):
                reply = '%.3f' % (status[client]['kelvin'])
            elif (line == 'SETP?'):
                reply = '%.3f' % (status[client]['setp'])
            elif (line == 'RANGE?'):
                reply = '%d' % (status[client]['range'])
            elif (line == 'PID?'):
                reply = '%.2f %.2f %.2f' % (status[client]['p'], status[client]['i'], status[client]['d'])
            elif (line == 'INNAME?'):
                reply = '%s %s' % (status[client]['nameA'], status[client]['nameB'])
     

            elif (len(args) > 1):
                try:
                    if (args[0] == 'SETP'):
                        val1 = float(args[1])
                        status[client]['setp'] = clip(val1)
                    elif (args[0] == 'RANGE'):
                        val1 = float(args[1])
                        status[client]['range'] = clip(val1)
                    elif (args[0] == 'INNAMEA'):
                        status[client]['nameA'] = args[1]
                    elif (args[0] == 'INNAMEB'):
                        status[client]['nameB'] = args[1]

                    elif (args[0] == 'TLIMIT'):
                        val1 = float(args[1])
                        status[client]['limit'] = clip(val1)

                    elif (args[0] == 'PID'):
                        val1 = float(args[1])
                        val2 = float(args[2])
                        val3 = float(args[3])
                        status[client]['p'] = clip(val1)
                        status[client]['i'] = clip(val2)
                        status[client]['d'] = clip(val3)

                    else:
                      break
                    repy = None

                except:
                    pass
            if (reply):
                self.wfile.write(reply + '\r\n')

class Server(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    daemon_threads = True
    allow_reuse_address = True
    def __init__(self, server_address, RequestHandlerClass):
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)

server = Server(('0.0.0.0', 24743), DummyDevice)
# terminate with Ctrl-C
try:
    server.serve_forever()
except KeyboardInterrupt:
    sys.exit(0)
