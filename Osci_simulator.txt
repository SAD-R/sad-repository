COMMANDS = [["identification", "*IDN?***", 1], ["mean value", "MEAN***", 2], ["voltage", "VOLT***", 3], ["voltage waveform", "WAVEVOLT***", 4], ["set voltage", "SETVOLT***", 5], ["frequency", "FREQ***", 6]]

STATUSES = [["identification", FIXED, "HAMEG,HMO3034,020398635,05.407\r\n"], ["mean value", CUSTOM, "mean_value()"], ["voltage", CUSTOM, "voltage()"], ["voltage waveform", CUSTOM, "voltage_waveform()"], ["set voltage", CUSTOM, "set_voltage()"], ["frequency", CUSTOM, "frequency()"]]


### User-custom code
m = 0.5     ##mean value
v = 0.0     ##voltage
u = 1
i = 0
j = 0
k = 0
w = [1]*100
z = [0]*100
pi = 3.14159
f = 1

######## FUNCTIONS ###########

def clip(s):
    if (s > 300): return 300
    if (s < 0.0): return 0
    return s

def mean_value():
	
	global m

	m = random.gauss(u/2, u/50)
	return "%.3f\r\n" %(m)

def set_voltage():

	global u
 
	args = COMMAND_RECEIVED.split()

	if len(args)>1:
		u = clip(float(args[1]))

def frequency():

	global f
 
	args = COMMAND_RECEIVED.split()

	if len(args)>1:
		f = clip(float(args[1]))

def voltage():
	
	global j

	v = math.sin(j*2*pi*f/100)    ## getting 100 samples/period
	j = j + 1	

	if (v >= 0):
		v = u + random.uniform(0.01,0.05)	
		return "%.3f\r\n" %(v)
	else:
		v = random.uniform(0.01,0.05)	
		return "%.3f\r\n" %(v)

def voltage_waveform():

	global w
	global z
	global i
	global k

	for i in range(len(w)):
		w[i] = random.gauss(u, u/25)

	for k in range(len(z)):
		z[k] = random.uniform(0, 0.1)

	return "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\r\n" %(w[0],w[1],w[2],w[3],w[4],w[5],w[6],w[7],w[8],w[9],z[0],z[1],z[2],z[3],z[4],z[5],z[6],z[7],z[8],z[9],w[10],w[11],w[12],w[13],w[14],w[15],w[16],w[17],w[18],w[19],z[10],z[11],z[12],z[13],z[14],z[15],z[16],z[17],z[18],z[19],w[20],w[21],w[22],w[23],w[24],w[25],w[26],w[27],w[28],w[29],z[20],z[21],z[22],z[23],z[24],z[25],z[26],z[27],z[28],z[29],w[30],w[31],w[32],w[33],w[34],w[35],w[36],w[37],w[38],w[39],z[30],z[31],z[32],z[33],z[34],z[35],z[36],z[37],z[38],z[39],w[40],w[41],w[42],w[43],w[44],w[45],w[46],w[47],w[48],w[49],z[40],z[41],z[42],z[43],z[44],z[45],z[46],z[47],z[48],z[49],w[50],w[51],w[52],w[53],w[54],w[55],w[56],w[57],w[58],w[59],z[50],z[51],z[52],z[53],z[54],z[55],z[56],z[57],z[58],z[59],w[60],w[61],w[62],w[63],w[64],w[65],w[66],w[67],w[68],w[69],z[60],z[61],z[62],z[63],z[64],z[65],z[66],z[67],z[68],z[69],w[70],w[71],w[72],w[73],w[74],w[75],w[76],w[77],w[78],w[79],z[70],z[71],z[72],z[73],z[74],z[75],z[76],z[77],z[78],z[79],w[80],w[81],w[82],w[83],w[84],w[85],w[86],w[87],w[88],w[89],z[80],z[81],z[82],z[83],z[84],z[85],z[86],z[87],z[88],z[89],w[90],w[91],w[92],w[93],w[94],w[95],w[96],w[97],w[98],w[99],z[90],z[91],z[92],z[93],z[94],z[95],z[96],z[97],z[98],z[99])	

	
	
