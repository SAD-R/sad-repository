#!../../bin/linux-x86_64/first

## You may have to change first to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/first.dbd"
first_registerRecordDeviceDriver pdbbase

## Configure devices
epicsEnvSet ("STREAM_PROTOCOL_PATH", ".:../protocols")

##Line below for LS336
drvAsynIPPortConfigure("PortA","192.168.88.184:7777",0,0,0) 
##Line below for MCAG-EPICS6: 
#drvAsynIPPortConfigure("PortA","192.168.88.181:5065",0,0,0)

## Load record instances
dbLoadRecords("db/first.db","user=ikergonzalezHost")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=ikergonzalezHost"

