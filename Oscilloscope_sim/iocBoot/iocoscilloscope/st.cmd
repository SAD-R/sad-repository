#!../../bin/linux-x86_64/osci_sim

## You may have to change oscilloscope to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/oscilloscope.dbd"
oscilloscope_registerRecordDeviceDriver pdbbase

##Configure devices
epicsEnvSet("STREAM_PROTOCOL_PATH",".:../protocols")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","65536")

##Line below for MCAG-EPICS6: 
drvAsynIPPortConfigure("PortA","192.168.88.181:9999",0,0,0)
## Line below for SE-RASPI-1
#drvAsynIPPortConfigure("PortA","192.168.88.188:5024",0,0,0)

## Load record instances
dbLoadRecords("db/oscilloscope.db","user=ikergonzalezHost")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=ikergonzalezHost"
